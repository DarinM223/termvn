extern crate ncurses;
extern crate termvn;

use ncurses::*;
use termvn::VnEngine;

fn main() {
    let mut engine = VnEngine::new(KEY_LEFT, 200);
    engine.run();
}
