use ncurses::*;
use std::time::Instant;
use super::{Event, destroy_win, Renderable};

pub struct Main {
    window: WINDOW,
}

impl Main {
    pub fn new(window: WINDOW) -> Main {
        Main { window: window }
    }
}

impl Renderable for Main {
    fn update(&mut self, curr_time: &Instant) {
        mvwprintw(self.window, 1, 1, "Hello world!");
    }

    fn clear(&self) {}

    fn refresh(&self) {
        box_(self.window, 0, 0);
        wrefresh(self.window);
    }

    fn handle_event(&mut self, event: &Option<Event>) -> Option<Event> {
        None
    }

    fn destroy(&mut self) {
        destroy_win(self.window);
    }
}
