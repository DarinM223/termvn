extern crate ncurses;
extern crate time;

pub mod dialogue;
pub mod main;

use dialogue::Dialogue;
use main::Main;
use ncurses::*;
use std::borrow::Cow;
use std::mem;
use std::str;
use std::time::Instant;

const ENTER_KEY: i32 = 10;
const DIALOGUE_WINDOW_HEIGHT_PERCENT: f64 = 0.2;

pub enum Event {
    KeyPress(i32),
}

pub trait Renderable {
    fn refresh(&self);
    fn clear(&self);
    fn update(&mut self, curr_time: &Instant);
    fn handle_event(&mut self, event: &Option<Event>) -> Option<Event>;
    fn destroy(&mut self);
}

pub struct VnEngine {
    /// The escape character value.
    escape_ch: i32,
    /// The delay between characters when rendering dialogue.
    delay: i64,
    /// Handles the updating of the dialogue window.
    dialogue: Dialogue,
    /// Handles the updating of the main window.
    main: Main,
    /// Max x bound of the terminal screen.
    max_x: i32,
    /// Max y bound of the terminal screen.
    max_y: i32,
}

impl VnEngine {
    pub fn new(escape_ch: i32, delay: i64) -> VnEngine {
        let win = initscr();

        raw();
        noecho();
        cbreak();
        keypad(win, true);
        nodelay(win, true);
        curs_set(CURSOR_VISIBILITY::CURSOR_INVISIBLE);

        let (mut max_x, mut max_y) = (0, 0);
        getmaxyx(stdscr(), &mut max_y, &mut max_x);
        let main_window = create_main_win(max_x, max_y, DIALOGUE_WINDOW_HEIGHT_PERCENT);
        let dialogue_window = create_dialogue_win(max_x, max_y, DIALOGUE_WINDOW_HEIGHT_PERCENT);

        let main = Main::new(main_window);
        let dialogue = Dialogue::new(dialogue_window, delay);

        VnEngine {
            escape_ch: escape_ch,
            delay: delay,
            main: main,
            dialogue: dialogue,
            max_x: max_x,
            max_y: max_y,
        }
    }

    /// Applies an event to the renderable objects and recursively
    /// applies the events that the objects may generate.
    pub fn apply_event(&mut self, event: &Option<Event>) {
        if event.is_none() {
            return;
        }

        let result = self.dialogue.handle_event(event);
        self.apply_event(&result);

        let result = self.main.handle_event(event);
        self.apply_event(&result);
    }

    pub fn run(&mut self) {
        loop {
            let curr_time = Instant::now();
            self.main.update(&curr_time);
            self.dialogue.update(&curr_time);

            let ch = getch();
            match ch {
                ch if ch == self.escape_ch => break,
                ENTER_KEY => self.apply_event(&Some(Event::KeyPress(ENTER_KEY))),
                _ => {}
            }

            self.main.refresh();
            self.dialogue.refresh();
        }
    }
}

impl Drop for VnEngine {
    fn drop(&mut self) {
        self.main.destroy();
        self.dialogue.destroy();
        endwin();
    }
}

fn create_main_win(max_x: i32, max_y: i32, dialogue_height_percentage: f64) -> WINDOW {
    let main_height_percentage = 1. - dialogue_height_percentage;
    let height = ((max_y as f64) * main_height_percentage).floor() as i32;
    let (start_y, start_x) = (0, 0);
    let win = newwin(height, max_x, start_y, start_x);
    box_(win, 0, 0);
    wrefresh(win);

    win
}

fn create_dialogue_win(max_x: i32, max_y: i32, height_percentage: f64) -> WINDOW {
    let height = ((max_y as f64) * height_percentage).floor() as i32;
    let (start_y, start_x) = (max_y - height, 0);
    let win = newwin(height, max_x, start_y, start_x);
    box_(win, 0, 0);
    wrefresh(win);

    win
}

pub fn destroy_win(win: WINDOW) {
    let ch = ' ' as chtype;
    wborder(win, ch, ch, ch, ch, ch, ch, ch, ch);
    wrefresh(win);
    delwin(win);
}
