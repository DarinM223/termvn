use ncurses::*;
use std::borrow::Cow;
use std::str;
use std::time::Instant;
use super::{Event, destroy_win, Renderable};
use time;

struct CurrentDialogue {
    text: Vec<u8>,
    person: String,
    delay: i64,
    index: usize,
    print_index: i32,
    window: WINDOW,
    last_updated: Option<Instant>,
}

impl CurrentDialogue {
    pub fn new(text: String, person: String, delay: i64, window: WINDOW) -> CurrentDialogue {
        CurrentDialogue {
            text: text.into_bytes(),
            person: person,
            delay: delay,
            index: 0,
            print_index: 0,
            window: window,
            last_updated: None,
        }
    }

    /// Returns the last time the dialogue was updated.
    pub fn last_time(&self) -> Option<Instant> {
        self.last_updated
    }

    /// Prints the text inside the dialogue window.
    pub fn print<'a, S>(&mut self, text: S)
        where S: Into<Cow<'a, str>>
    {
        let text = text.into().into_owned();
        for byte in text.bytes() {
            mvwaddch(self.window, 1, 1 + self.print_index, byte as u32);
            self.print_index += 1;
        }
    }

    /// Updates function called every tick. It checks the time duration
    /// from the last print time and prints a new character if the duration
    /// is greater than or equal to the delay.
    fn update(&mut self, curr_time: &Instant) {
        if self.is_complete() {
            return;
        }

        if let Some(last_time) = self.last_time() {
            let diff = time::Duration::from_std(curr_time.duration_since(last_time)).unwrap();
            if diff.num_milliseconds() >= self.delay {
                self.update_text(curr_time);
            }
        } else {
            self.update_text(curr_time);
        }
    }

    /// Returns true when all the text has finished loading, false otherwise.
    pub fn is_complete(&self) -> bool {
        self.index >= self.text.len()
    }

    /// Skip the delay and immediately render the rest of the dialogue.
    pub fn complete_immediately(&mut self) {
        let range = &self.text.clone()[self.index..];
        let text = String::from_utf8_lossy(range);
        self.print(text);
        self.index = self.text.len();
    }

    fn update_text(&mut self, curr_time: &Instant) {
        let text = format!("{}", self.text[self.index] as char);
        self.print(text);
        self.index += 1;
        self.last_updated = Some(curr_time.clone());
    }
}

pub struct Dialogue {
    window: WINDOW,
    delay: i64,
    curr: Option<CurrentDialogue>,
}

impl Dialogue {
    pub fn new(window: WINDOW, delay: i64) -> Dialogue {
        let mut dialogue = Dialogue {
            window: window,
            delay: delay,
            curr: None, // TODO(DarinM223): load dialogue from file.
        };

        dialogue.set_text("Hello world!", "Person");
        dialogue
    }

    pub fn set_text<'a, 'b, S1, S2>(&mut self, text: S1, person: S2)
        where S1: Into<Cow<'a, str>>,
              S2: Into<Cow<'b, str>>
    {
        self.clear();
        self.curr = Some(CurrentDialogue::new(text.into().into_owned(),
                                              person.into().into_owned(),
                                              self.delay,
                                              self.window));
    }
}

impl Renderable for Dialogue {
    fn update(&mut self, curr_time: &Instant) {
        self.curr.as_mut().map(|d| d.update(curr_time));
    }

    /// Clears the dialogue window.
    fn clear(&self) {
        wclear(self.window);
    }

    /// Refresh code called every tick to refresh the window.
    fn refresh(&self) {
        box_(self.window, 0, 0);
        wrefresh(self.window);
    }

    fn handle_event(&mut self, event: &Option<Event>) -> Option<Event> {
        if !self.curr.as_ref().map_or(true, |d| d.is_complete()) {
            self.curr.as_mut().map(|d| d.complete_immediately());
        } else {
            // TODO(DarinM223): load the next text to render.
            self.set_text("Blah", "Person");
        }

        None
    }

    fn destroy(&mut self) {
        destroy_win(self.window);
    }
}
